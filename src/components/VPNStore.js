import { observable } from 'mobx'
import _ from 'lodash/fp'

export default class VPNStore {
  constructor(vpnList) {
    this.list = vpnList
  }
  headers = [
    {
      head: 'VPN Service',
      label: "VPN Service"
    },
    {
      head: 'Jurisdiction',
      text: 'Based In (Country)',
      label: "Based in (Country)"
    },
    {
      head: 'Jurisdiction',
      text: 'Fourteen Eyes?',
      label: "\"Fourteen Eyes\" Country",
      good: ["No"],
      warning: ["Cooperative", "Fourteen", "Nine", "See Note"],
      bad: ["Five", "Not Disclosed"]
    },
    {
      head: 'Jurisdiction',
      text: 'Enemy of the Internet',
      label: "Enemy of the Internet",
      good: ["No"],
      warning: ["Owned", "See Note"],
      bad: ["Yes", "Not Disclosed"]
    },
    {
      head: 'Logging',
      text: 'Logs Traffic',
      label: "Traffic",
      good: ["No"],
      warning: ["See Note"],
      bad: ["Yes"]
    },
    {
      head: 'Logging',
      text: 'Logs DNS Requests',
      label: "DNS Requests",
      good: ["No"],
      warning: ["See Note"],
      bad: ["Yes"]
    },
    {
      head: 'Logging',
      text: 'Logs Timestamps',
      label: "Timestamps",
      good: ["No"],
      warning: ["See Note"],
      bad: ["Yes"]
    },
    {
      head: 'Logging',
      text: 'Logs Bandwidth',
      label: "Bandwidth",
      good: ["No"],
      warning: ["See Note"],
      bad: ["Yes"]
    },
    {
      head: 'Logging',
      text: 'Logs IP Address',
      label: "IP Address",
      good: ["No"],
      warning: ["See Note"],
      bad: ["Yes"]
    },
    {
      head: 'Activism',
      text: 'Anonymous Payment Method',
      label: "Anonymous Payment Method",
      good: ["Yes"],
      warning: ["Email", "See Note"],
      bad: ["No"]
    },
    {
      head: 'Activism',
      text: 'Accepts Bitcoin',
      label: "Accepts Bitcoin",
      good: ["Yes"],
      warning: ["See Note"],
      bad: ["No"]
    },
    {
      head: 'Activism',
      text: 'PGP key Available',
      label: "PGP Key Available",
      good: ["Yes"],
      warning: ["See Note"],
      bad: ["No"]
    },
    {
      head: 'Activism',
      text: 'Gives back to Privacy Causes',
      label: "Gives back to Privacy Causes",
      good: ["Yes"],
      warning: ["See Note"],
      bad: ["No"]
    },
    {
      head: 'Activism',
      text: 'Meets PrivacyTools IO Criteria',
      label: "Meets PrivacyToolsIO Criteria",
      good: ["Yes"],
      warning: ["See Note"],
      bad: ["No"]
    },
    {
      head: 'Leak Protection',
      text: '1st Party DNS Servers',
      label: "1st Party DNS Servers",
      good: ["Yes"],
      warning: ["See Note"],
      bad: ["No"]
    },
    {
      head: 'Leak Protection',
      text: 'IPv6 Supported / Blocked',
      label: "IPv6 Supported / Blocked",
      good: ["Yes"],
      warning: ["See Note"],
      bad: ["No"]
    },
    {
      head: 'Protocols',
      text: 'Offers OpenVPN',
      label: "Offers OpenVPN",
      good: ["Yes"],
      warning: ["See Note"],
      bad: ["No"]
    },
    {
      head: 'Obfuscation',
      text: 'Supports Multihop',
      label: "Supports Multihop",
      good: ["Yes"],
      warning: ["See Note"],
      bad: ["No"]
    },
    {
      head: 'Obfuscation',
      text: 'Supports TCP Port 443',
      label: "Supports TCP Port 443",
      good: ["Yes"],
      warning: ["See Note"],
      bad: ["No"]
    },
    {
      head: 'Obfuscation',
      text: 'Supports Obfsproxy',
      label: "Supports Obfsproxy",
      good: ["Yes"],
      warning: ["See Note"],
      bad: ["No"]
    },
    {
      head: 'Obfuscation',
      text: 'Supports SOCKS',
      label: "Supports SOCKS",
      good: ["Yes"],
      warning: ["See Note"],
      bad: ["No"]
    },
    {
      head: 'Obfuscation',
      text: 'Supports SSL Tunnel',
      label: "Supports SSL Tunnel",
      good: ["Yes"],
      warning: ["See Note"],
      bad: ["No"]
    },
    {
      head: 'Obfuscation',
      text: 'Supports SSH Tunnel',
      label: "Supports SSH Tunnel",
      good: ["Yes"],
      warning: ["See Note"],
      bad: ["No"]
    },
    {
      head: 'Obfuscation',
      text: 'Other Proprietary Protocols',
      label: "Other Proprietary Protocols",
      good: [],
      warning: ["Yes", "See Note"],
      bad: []
    },
    {
      head: 'Port Blocking',
      text: 'Auth SMTP',
      label: "Blocks SMTP (Authent.)",
      good: ["No"],
      warning: ["Some", "See Note"],
      bad: ["Yes"]
    },
    {
      head: 'Port Blocking',
      text: 'P2P',
      label: "Blocks P2P",
      good: ["No"],
      warning: ["Some", "See Note"],
      bad: ["Yes"]
    },
    {
      head: 'Speeds',
      text: 'US Server Average (%)',
      label: "US Server DL Speeds (%)"
    },
    {
      head: 'Speeds',
      text: 'Int\'l Server Average (%)',
      label: "International Server DL Speeds (%)"
    },
    {
      head: 'Servers',
      text: 'Dedicated or Virtual',
      label: "Dedicated or Virtual",
      good: ["Dedicated"],
      warning: ["Mix", "See Note"],
      bad: ["Virtual"]
    },
    {
      head: 'Security',
      text: 'Default Data Encryption',
      label: "Default Provided Encryption",
      good: ["AES-256", "AES-128"],
      warning: ["Blowfish-128", "Blowfish-256", "See Note"],
      bad: ["Blowfish-64"]
    },
    {
      head: 'Security',
      text: 'Strongest Data Encryption',
      label: "Strongest Provided Encryption",
      good: ["AES-256", "AES-128"],
      warning: ["Blowfish-128", "Blowfish-256", "See Note"],
      bad: ["Blowfish-64", "MPPE-128"]
    },
    {
      head: 'Security',
      text: 'Weakest Handshake Encryption',
      label: "Weakest Provided Handshake",
      good: ["RSA-4096", "RSA-2048", "DH-2048"],
      warning: ["See Note"],
      bad: ["MS-CHAPv2", "MSCHAP-v2", "RSA-RC4", "RSA-1024"]
    },
    {
      head: 'Security',
      text: 'Strongest Handshake Encryption',
      label: "Strongest Provided Handshake",
      good: ["RSA-4096", "RSA-2048", "CA-4096", "DH-2048"],
      warning: ["See Note"],
      bad: ["MS-CHAPv2", "MSCHAP-v2", "RSA-RC4", "RSA-1024"]
    },
    {
      head: 'Availability',
      text: '# of Connections',
      label: "# of Simultaneous Connections"
    },
    {
      head: 'Availability',
      text: '# of Countries',
      label: "# of Countries"
    },
    {
      head: 'Availability',
      text: '# of Servers',
      label: "# of Servers"
    },
    {
      head: 'Support',
      text: 'Linux (Manual Config)',
      label: "Linux Support (Manual)",
      good: ["Yes"],
      warning: ["Partial", "See Note"],
      bad: ["No"]
    },
    {
      head: 'Website',
      text: '# of Persistent Cookies',
      label: "# of Persistent Cookies"
    },
    {
      head: 'Website',
      text: '# of External Trackers',
      label: "# of External Trackers"
    },
    {
      head: 'Website',
      text: '# of Proprietary APIs',
      label: "# of Proprietary API's"
    },
    {
      head: 'Website',
      text: 'Server SSL Rating',
      label: "Server SSL Rating",
      good: ["A+", "A", "A-"],
      semiGood: ["B"],
      warning: ["C", "See Note"],
      bad: ["F", "T", "No SSL Cert"]
    },
    {
      head: 'Website',
      text: 'SSL Cert issued to',
      label: "SSL Certificate issued to",
      good: ["Self"],
      warning: ["See Note"],
      bad: ["No SSL Cert", "CloudFlare", "Incapsula"]
    },
    {
      head: 'Pricing',
      text: '$ / Month (Annual Pricing)',
      label: "$ / Month - (Annual Pricing)"
    },
    {
      head: 'Pricing',
      text: '$ / Connection / Month',
      label: "$ / Connection / Month"
    },
    {
      head: 'Pricing',
      text: 'Free Trial',
      label: "Free Trial Available",
      good: ["Yes", "Free"],
      warning: ["See Note"],
      bad: ["No"]
    },
    {
      head: 'Pricing',
      text: 'Refund Period (Days)',
      label: "Refund Window (Days)"
    },
    {
      head: 'Ethics',
      text: 'Contradictory Logging Policies',
      label: "Contradictory Logging Policies",
      good: ["No"],
      warning: ["See Note"],
      bad: ["Yes"]
    },
    {
      head: 'Ethics',
      text: 'Falsely Claims 100% Effective',
      label: "Falsely Claims Service is 100% Effective",
      good: ["No"],
      warning: ["See Note"],
      bad: ["Yes"]
    },
    {
      head: 'Ethics',
      text: 'Incentivizes Social Media Spam',
      label: "Incentivizes social media spam",
      good: ["No"],
      warning: ["See Note"],
      bad: ["Yes"]
    },
    {
      head: 'Policies',
      text: 'Forbids Spam',
      label: "Forbids spam",
      good: ["Yes"],
      warning: ["Some", "See Note"],
      bad: ["No"]
    },
    {
      head: 'Policies',
      text: 'Require Ethical Copy',
      label: "Requires ethical copy",
      good: ["Yes"],
      warning: ["Some", "See Note"],
      bad: ["No"]
    },
    {
      head: 'Policies',
      text: 'Requires Full Disclosure',
      label: "Requires full disclosure",
      good: ["Yes"],
      warning: ["Some", "See Note"],
      bad: ["No"]
    },
    {
      head: 'Policies',
      text: 'Practice Ethical Copy',
      label: "Practice ethical copy",
      good: ["Yes"],
      warning: ["Some", "See Note"],
      bad: ["No"]
    },
    {
      head: 'Policies',
      text: 'Gives Full Disclosure',
      label: "Give full disclosure",
      good: ["Yes"],
      warning: ["Some", "See Note"],
      bad: ["No"]
    }
  ]
  @observable sortedList = this.list
}