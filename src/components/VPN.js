import Inferno from 'inferno'
import Component from 'inferno-component'
import { observer } from 'inferno-mobx'
import classNames from 'classnames/bind'
import _ from 'lodash/fp'
import styles from './VPN.css'
import VPNStore from './VPNStore'
import VPNList from './VPNList.json'

const cn = classNames.bind(styles)

const store = new VPNStore(VPNList["Sheet1"])

const TableHead = observer(({ headers }) => {
  return (
    <tr>
      {headers.map((obj) => {
        return (
          <th className={styles.tableHeader}>
            <div>{_.toUpper(obj.head)}</div>
            <div>{obj.text}</div>
          </th>
        )
      })}
    </tr>
  )
})

const TableRow = observer(({ obj, headers }) => {
  return (
    <tr className={styles.tableRow}>
      {headers.map((header) => {
        return (
          <td className={cn({
            good: _.indexOf(obj[header.label])(header.good) !== -1,
            semiGood: _.indexOf(obj[header.label])(header.semiGood) !== -1,
            warning: _.indexOf(obj[header.label])(header.warning) !== -1,
            bad: _.indexOf(obj[header.label])(header.bad) !== -1,
            missing: obj[header.label] === undefined
          })}
        >{obj[header.label]}</td>)
      })}
    </tr>
  )
})

@observer
class VPN extends Component {
  render() {
    return (
      <div className={styles.container}>
        <table>
          <TableHead headers={store.headers} />
          {store.sortedList.map((obj) => {
            return <TableRow
                      key={_.camelCase(obj["VPN Service"])}
                      obj={obj}
                      headers={store.headers}
                    />
          })}
        </table>
      </div>
    )
  }
}

export default VPN
