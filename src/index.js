import Inferno from 'inferno'
import VPN from './components/VPN'
import './index.css'

Inferno.render(
  <VPN />,
  document.getElementById('app')
)
