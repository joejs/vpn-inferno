Ejected from Create Inferno App

yarn add:
  lodash
  mobx
  inferno-mobx
  styled-components

yarn add -D:
  babel-plugin-transform-decorators-legacy
  image-webpack-loader
  babel-plugin-lodash
  <!--lodash-webpack-plugin-->
  postcss-cssnext
  eslint-plugin-lodash-fp

test .js and babel-loader:
query: {
  plugins: ['transform-decorators-legacy', 'lodash'],
}

test .css, add CSS Modules and localIdentName
{
  test: /\.css$/,
  loader: 'style!css?modules&importLoaders=1&localIdentName=[name]--[local]--[hash:base64:5]!postcss'
}

image optimization:
{
  test:  /.*\.(gif|png|jpe?g|svg)$/i,
  loaders: [
    'file?name=static/media/[name].[hash:8].[ext]',
    'image-webpack?{progressive:true, optimizationLevel: 7, interlaced: false, pngquant:{quality: "65-90", speed: 4}, mozjpeg: {quality: 65}}',
  ]
}

Replace SVG match with the new image test regex

var cssnext = require('postcss-cssnext');
replace autoprefixer with cssnext in postcss function

import sanitize.css or normalize.css

jsconfig.json:
{
    "compilerOptions": {
        "target": "ES6",
        "experimentalDecorators": true
    },
    "exclude": [
        "node_modules"
    ]
}

VSCode user settings:
{
    "editor.tabSize": 2,
    "editor.renderWhitespace": "boundary",
    "css.validate": false
}

"eslintConfig": {
  "plugins": [
      "lodash-fp"
  ],
  "extends": [
    "react-app",
    "plugin:lodash-fp/recommended"
  ]
}

This breaks lodash/fp for cherry-picked builds:
var LodashModuleReplacementPlugin = require('lodash-webpack-plugin');
module.plugins: [
  new LodashModuleReplacementPlugin(),
]